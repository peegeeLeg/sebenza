#include "recipients.h"
#include "ui_recipients.h"

// -------------------------------------
// General Functions
// -------------------------------------

/*!
 * \brief Recipients::Recipients
 * \param parent
 */
Recipients::Recipients(QWidget *parent) :
  QDialog(parent, Qt::CustomizeWindowHint),
  ui(new Ui::Recipients)
{


  // set up window and set parent
  ui->setupUi(this);
  this->setWindowFlags(Qt::WindowStaysOnTopHint);

  ui->rs_tb_biometric_capture->setIcon(QIcon(":/Company/Logo/FingerScan.png"));
  ui->rs_tb_biometric_capture->setIconSize(ui->rs_tb_biometric_capture->size());

  // setup all application screens
  newFingerprintScannerWindow = new FingerprintScanner;

  add_recipient_template = NULL;

  // Change colour of frame to white
  QPalette pal = this->palette();
  pal.setColor(QPalette::Window, Qt::white);
  this->setPalette(pal);

//  ui->rs_grp_tv
}

/*!
 * \brief Recipients::~Recipients
 */
Recipients::~Recipients()
{
  delete ui;
}

/*!
 * \brief Recipients::setTypeScreen
 * \param action
 * \param recipientCatergory
 */
void Recipients::setTypeScreen(adminAction action,\
                               recipientType recipientCatergory)
{
  QString temp_str;
  ui->rs_tb_biometric_capture->setIcon(QIcon(":/Company/Logo/FingerScan.png"));
  ui->rs_tb_biometric_capture->setIconSize(ui->rs_tb_biometric_capture->size());
  ui->rs_lbl_info_error->clear();
  ui->rs_lbl_fp_capture_status->clear();

  // check recipient type
  switch (recipientCatergory){
  case Learner:
    // set widgets
    ui->rs_frame_learners->setVisible(true);
    ui->rs_frame_regions->setVisible(false);

    switch (action) {
    case AddUser:
      // set widgets
      ui->rs_pb_learner_save->setVisible(true);
      ui->rs_pb_learner_cancel->setVisible(true);
      ui->rs_pb_learner_delete->setVisible(false);
      ui->rs_pb_learner_search->setVisible(false);
      ui->rs_tb_biometric_capture->setEnabled(true);

      // set
      currentAdminAction  = AddUser;
      ui->rs_learner_lbl_title_info->setText("Add Recipient");

      // get all regions
      current_regions_list.clear();
      DatabaseController::active_db_handle_->getAllRecipientsRegions(current_regions_list);

      // populate desticts
      for(int i = 0; i < current_regions_list.size(); i++){
        ui->rs_rgn_and_ln_list->addItem(current_regions_list.at(i).district);
      }

      break;

    case EditUser:
      // set
      currentAdminAction  = EditUser;
      ui->rs_learner_lbl_title_info->setText("Edit Recipient");


      // set widgets
      ui->rs_pb_learner_save->setVisible(true);
      ui->rs_pb_learner_cancel->setVisible(true);
      ui->rs_pb_learner_delete->setVisible(false);
      ui->rs_pb_learner_search->setVisible(false);
      ui->rs_tb_biometric_capture->setEnabled(true);

      // get all recipients
      recipients_list.clear();
      DatabaseController::active_db_handle_->getAllRecipients(recipients_list);

      // populate recipients

      for(int i = 0; i < recipients_list.size(); i++){

        DatabaseController::active_db_handle_->getSchoolNameById(temp_str, recipients_list.at(i).school_id);

        ui->rs_rgn_and_ln_list->addItem(recipients_list.at(i).name    +
                                        " "                                 +
                                        recipients_list.at(i).surname +
                                        " | "                               +
                                        temp_str);
        temp_str = "";
      }
      break;

    case DeleteUser:
      // set
      currentAdminAction  = DeleteUser;
      ui->rs_learner_lbl_title_info->setText("Delete Recipient");

      // set widgets
      ui->rs_pb_learner_save->setVisible(false);
      ui->rs_pb_learner_cancel->setVisible(true);
      ui->rs_pb_learner_delete->setVisible(true);
      ui->rs_pb_learner_search->setVisible(false);
      ui->rs_tb_biometric_capture->setEnabled(false);

      // get all recipients
      recipients_list.clear();
      DatabaseController::active_db_handle_->getAllRecipients(recipients_list);

      // populate recipients

      for(int i = 0; i < recipients_list.size(); i++){

        DatabaseController::active_db_handle_->getSchoolNameById(temp_str, recipients_list.at(i).school_id);

        ui->rs_rgn_and_ln_list->addItem(recipients_list.at(i).name    +
                                        " "                                 +
                                        recipients_list.at(i).surname +
                                        " | "                               +
                                        temp_str);
        temp_str = "";
      }
      break;

    case AuthenticateUser:
      break;
    }

    break;



  case Group:
    // set widgets
    ui->rs_frame_learners->setVisible(false);
    ui->rs_frame_regions->setVisible(true);

    // set secondary widgets
    switch (action) {
    case AddUser:
      // set widgets
      ui->rs_pb_grp_save->setVisible(true);
      ui->rs_pb_grp_cancel->setVisible(true);
      ui->rs_pb_grp_delete->setVisible(false);

      break;

    case EditUser:
      // set widgets
      ui->rs_pb_grp_save->setVisible(true);
      ui->rs_pb_grp_cancel->setVisible(true);
      ui->rs_pb_grp_delete->setVisible(false);
      break;

    case DeleteUser:
      // set widgets
      ui->rs_pb_grp_save->setVisible(false);
      ui->rs_pb_grp_cancel->setVisible(true);
      ui->rs_pb_grp_delete->setVisible(true);
      break;
    case AuthenticateUser:
      break;
    }

    // break main switch
    break;
  }
}


/*!
 * \brief Recipients::addRecipient
 * \return true if sys user added successfully
 *         false if sys user failed to be added
 */
bool Recipients::addRecipient()
{
  // result variable
  int result;

  // QString
  RecipientsDetails* detailsOfRecipient     = new RecipientsDetails;

  // collect inputs
  detailsOfRecipient->name                  = ui->rs_le_name->text();
  detailsOfRecipient->surname               = ui->rs_le_surname->text();
  detailsOfRecipient->address               = ui->rs_le_address->text();
  detailsOfRecipient->contact_number        = ui->rs_le_contact->text();
  detailsOfRecipient->id_number             = ui->rs_le_id->text();
  detailsOfRecipient->gender                = ui->rs_cmBx_gender->currentText();
  detailsOfRecipient->recipient_type        = getStringToRecipientType(ui->rs_cmBx_type->currentText());
  detailsOfRecipient->birth_date            = ui->rs_dateTimeEdit_dob->date();
  detailsOfRecipient->fingerprint_template  = add_recipient_template;
  detailsOfRecipient->fingerprint_enrolled  = (ui->rs_cb_no_fingerprint->isChecked()? false:true);

  detailsOfRecipient->region_id = 0;
  DatabaseController::active_db_handle_->getIdByDistricName(ui->rs_cmBx_district->currentText(),
                                                            detailsOfRecipient->region_id );

  detailsOfRecipient->school_id = 0;
  DatabaseController::active_db_handle_->getIdBySchoolName(ui->rs_cmBx_school->currentText(),
                                                           detailsOfRecipient->school_id );

  // validate inputs
  if(detailsOfRecipient->name.trimmed()     == "" ||
     detailsOfRecipient->surname.trimmed()  == ""){

    //- display no error
    ui->rs_lbl_info_error->setText(errorMsg("Enter all required fields!"));

    return false;
  }

  // edit sys user
  result = DatabaseController::active_db_handle_->addRecipient(detailsOfRecipient);

  // assess result
  if(result == 0){

    //- display message
    ui->rs_lbl_info_error->setText(successMsg("Recipient added succefully!"));

    DatabaseController::active_db_handle_->trail(QString("Added recipient called %1 %2")
                                                 .arg(detailsOfRecipient->name + " " + detailsOfRecipient->surname)
                                                 .arg((detailsOfRecipient->fingerprint_enrolled == true ?
                                                         ", And enrolled a fingerprint":
                                                         "")));

    return true;
  }
  else if (result == -1){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Enter all required fields"));
  }
  else if (result == -3){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Please enroll recipients fingerprint"));
  }
  else if (result == -4){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("System error. Contact Service Provider"));
  }
  else{
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Unknown Add System User error. Contact Service Provider"));
  }

  // return
  return false;

}

/*!
 * \brief Recipients::removeRecipient
 * \return true if sys user removed successfully
 *         false if sys user failed to be removed
 */
bool Recipients::removeRecipient()
{
  // result variable
  int result;

  // edit sys user
  result = DatabaseController::active_db_handle_->deleteRecipient(SystemUser::current_recipient_details.id);

  // assess result
  if(result == 0){

    //- display message
    ui->rs_lbl_info_error->setText(successMsg("Recipient removed succefully!"));

    DatabaseController::active_db_handle_->trail(QString("Removed recipient called %1")
                                                 .arg(SystemUser::current_recipient_details.name +
                                                      " " + SystemUser::current_recipient_details.surname));

    return true;
  }
  else if (result == -1){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("System error deleting recipient details. Contact Service Provider"));
  }
  else if (result == -3){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("System error deleting recipient template. Contact Service Provider"));
  }
  else{
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Unknown Add System User error. Contact Service Provider"));
  }

  // return
  return false;
}

/*!
 * \brief Recipients::editRecipient
 * \return true if sys user editted successfully
 *         false if sys user failed to be editted
 */
bool Recipients::editRecipient()
{
  // result variable
  int result;

  // User details
  RecipientsDetails* detailsOfRecipient  = new RecipientsDetails;

  // collect inputs
  detailsOfRecipient->name                  = ui->rs_le_name->text();
  detailsOfRecipient->surname               = ui->rs_le_surname->text();
  detailsOfRecipient->address               = ui->rs_le_address->text();
  detailsOfRecipient->contact_number        = ui->rs_le_contact->text();
  detailsOfRecipient->id_number             = ui->rs_le_id->text();
  DatabaseController::active_db_handle_->getIdByDistricName(ui->rs_cmBx_district->currentText(),detailsOfRecipient->region_id);
  DatabaseController::active_db_handle_->getIdBySchoolName(ui->rs_cmBx_school->currentText(),detailsOfRecipient->school_id);
  detailsOfRecipient->gender                = ui->rs_cmBx_gender->currentText();
  detailsOfRecipient->recipient_type        = getStringToRecipientType(ui->rs_cmBx_type->currentText());
  detailsOfRecipient->birth_date            = ui->rs_dateTimeEdit_dob->date();
  detailsOfRecipient->fingerprint_template  = add_recipient_template;
  detailsOfRecipient->fingerprint_enrolled  = (add_recipient_template == NULL? SystemUser::current_recipient_details.fingerprint_enrolled:true);

  // Validate inputs
  if(SystemUser::current_recipient_details.id <= 0){

    //- display no error
    ui->rs_lbl_info_error->setText(errorMsg("Select Recipient!"));

    return false;
  }
  if(detailsOfRecipient->name.trimmed()     == "" ||
     detailsOfRecipient->surname.trimmed()  == ""){

    //- display no error
    ui->rs_lbl_info_error->setText(errorMsg("Enter all required fields!"));

    return false;
  }

  // add sys user
  result = DatabaseController::active_db_handle_->editRecipient(detailsOfRecipient,
                                                                SystemUser::current_recipient_details.id);

  // assess result
  if(result == 0){

    //- display message
    ui->rs_lbl_info_error->setText(successMsg("Recipient editted succefully!"));

    DatabaseController::active_db_handle_->trail(QString("Editted recipient called %1 %2")
                                                 .arg(detailsOfRecipient->name + " " + detailsOfRecipient->surname)
                                                 .arg((detailsOfRecipient->fingerprint_enrolled == true ?
                                                         ", And enrolled a fingerprint":
                                                         "")));
    return true;
  }
  else if (result == -1){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("user's Name or Surname not entered"));
  }
  else if (result == -3){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("fingerprint not enrolled"));
  }
  else if (result == -4 || result == -5){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("System error. Contact Service Provider"));
  }
  else{
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Unknown Edit System User error. Contact Service Provider"));
  }
  // return
  return false;
}


/*!
 * \brief UserAdministration::getAllSystemUsers
 * \param users_list
 * \param detailsOfUser
 * \return true if users list was retrived successfully
 *         false if users list failed to be retrived
 */
void Recipients::getAllRecipients(QList<RecipientsDetails>& recipient_list)
{
  recipient_list.clear();
  // result variable
  int result;

  // add sys user
  result = DatabaseController::active_db_handle_->getAllRecipients(recipient_list);

  // assess result
  if(result == 0){

    //- Populate the actual view
    ui->rs_rgn_and_ln_list->clear();

    for(int i = 0; i < recipient_list.size(); i++)
    {
      ui->rs_rgn_and_ln_list->addItem(recipient_list.at(i).name +
                                      " "                              +
                                      recipient_list.at(i).surname);

    }


  }
  else if (result == -1){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Enter both Username & Password"));
  }
  else if (result == -3){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Unknown Add System User error. Contact Service Provider"));
  }
}

void Recipients::setWidgets(bool enable)
{
  // set widgets
  ui->rs_le_name->setEnabled(enable);
  ui->rs_le_surname->setEnabled(enable);
  ui->rs_le_address->setEnabled(enable);
  ui->rs_le_contact->setEnabled(enable);
  ui->rs_le_id->setEnabled(enable);
}

// -------------------------------------
// LEARNER - Window
// -------------------------------------

void Recipients::on_rs_pb_learner_seach_clicked()
{
  // get name and surname for search
  QString name = ui->rs_le_name->text();
  QString surname = ui->rs_le_surname->text();

  // search
}

/////////////////////////////////////////////////////////////////////////////////////////////////
///
///
///
/*!
 * \brief Recipients::addStaff
 * \return true if Staff user added successfully
 *         false if Staff user failed to be added
 */
bool Recipients::addStaff()
{

  // result variable
  int result;

  // QString
  StaffDetails* detailsOfStaff     = new StaffDetails;

  // collect inputs
  detailsOfStaff->username                  = ui->rs_le_name->text();
  detailsOfStaff->fingerprint_template  = add_recipient_template;
  detailsOfStaff->fingerprint_enrolled  = (ui->rs_cb_no_fingerprint->isChecked()? false:true);


  // validate inputs
  if(detailsOfStaff->username.trimmed()     == "" ){

    //- display no error
    ui->rs_lbl_info_error->setText(errorMsg("Enter required field!"));

    return false;
  }

  // edit sys user
  result = DatabaseController::active_db_handle_->addStaff(detailsOfStaff);

  // assess result
  if(result == 0){

    //- display message
    ui->rs_lbl_info_error->setText(successMsg("Staff added succefully!"));

    DatabaseController::active_db_handle_->trail(QString("Added Staff with username %1 %2")
                                                 .arg(detailsOfStaff->username)
                                                 .arg((detailsOfStaff->fingerprint_enrolled == true ?
                                                         ", And enrolled a fingerprint":
                                                         "")));

    return true;
  }
  else if (result == -1){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Enter all required fields"));
  }
  else if (result == -3){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Please enroll Staff fingerprint"));
  }
  else if (result == -4){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("System error. Contact Service Provider"));
  }
  else{
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Unknown Add System User error. Contact Service Provider"));
  }

  // return
  return false;

}

/*!
 * \brief Recipients::removeStaff
 * \return true if sys user removed successfully
 *         false if sys user failed to be removed
 */
bool Recipients::removeStaff()
{
  // result variable
  int result;

  // edit sys user
  result = DatabaseController::active_db_handle_->deleteStaff(SystemUser::current_staff_details.id);

  // assess result
  if(result == 0){

    //- display message
    ui->rs_lbl_info_error->setText(successMsg("Staff removed succefully!"));

    DatabaseController::active_db_handle_->trail(QString("Removed Staff called %1")
                                                 .arg(SystemUser::current_staff_details.username));

    return true;
  }
  else if (result == -1){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("System error deleting Staff details. Contact Service Provider"));
  }
  else if (result == -3){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("System error deleting Staff template. Contact Service Provider"));
  }
  else{
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Unknown Add System User error. Contact Service Provider"));
  }

  // return
  return false;
}

/*!
 * \brief Recipients::editStaff
 * \return true if sys user editted successfully
 *         false if sys user failed to be editted
 */
bool Recipients::editStaff()
{
  // result variable
  int result;

  // User details
  StaffDetails* detailsOfStaff  = new StaffDetails;

  // collect inputs
  detailsOfStaff->username              = ui->rs_le_name->text();
  detailsOfStaff->fingerprint_template  = add_recipient_template;
  detailsOfStaff->fingerprint_enrolled  = (add_recipient_template == NULL? SystemUser::current_staff_details.fingerprint_enrolled:true);

  // Validate inputs
  if(SystemUser::current_staff_details.id <= 0){

    //- display no error
    ui->rs_lbl_info_error->setText(errorMsg("Select Staff!"));

    return false;
  }
  if(detailsOfStaff->username.trimmed()     == ""){

    //- display no error
    ui->rs_lbl_info_error->setText(errorMsg("Enter  required field!"));

    return false;
  }

  // add sys user
  result = DatabaseController::active_db_handle_->editStaff(detailsOfStaff,
                                                                SystemUser::current_staff_details.id);

  // assess result
  if(result == 0){

    //- display message
    ui->rs_lbl_info_error->setText(successMsg("Staff editted succefully!"));

    DatabaseController::active_db_handle_->trail(QString("Editted Staff with username %1 %2")
                                                 .arg(detailsOfStaff->username)
                                                 .arg((detailsOfStaff->fingerprint_enrolled == true ?
                                                         ", And enrolled a fingerprint":
                                                         "")));
    return true;
  }
  else if (result == -1){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("user's username not entered"));
  }
  else if (result == -3){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("fingerprint not enrolled"));
  }
  else if (result == -4 || result == -5){
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("System error. Contact Service Provider"));
  }
  else{
    //- display error
    ui->rs_lbl_info_error->setText(errorMsg("Unknown Edit System User error. Contact Service Provider"));
  }
  // return
  return false;
}



///////////////////////////////////////////////////////////////////////////////////////////////////

void Recipients::on_rs_tb_biometric_capture_pressed()
{
  template_quality = 0;
  add_recipient_template.clear();

  newFingerprintScannerWindow->capture(add_recipient_template,template_quality);

  ui->rs_tb_biometric_capture->setIcon(QIcon("just_captured.bmp"));
  ui->rs_tb_biometric_capture->setIconSize(ui->rs_tb_biometric_capture->size());

  // must delete stored image
  QDir dir;
  dir.remove("just_captured.bmp");

  if(template_quality <= MIN_FP_QUALITY)
  {
    //- must clear captured image
    add_recipient_template.clear();

    ui->rs_lbl_info_error->setText(errorMsg(QString("Fingerprint Quality too low %1. Expecting more than %2!")
                                            .arg(QString::number(template_quality))
                                            .arg(QString::number(MIN_FP_QUALITY))));
    ui->rs_lbl_fp_capture_status->setText(errorMsg("POOR Quality!"));
    return;
  }
  else
  {
    ui->rs_lbl_fp_capture_status->setText(successMsg("Good Quality!"));
  }


  // check fingerprint for duplication
  // get all ids of the stored template
  QList<int>            template_ids_list;
  QPair<QByteArray,int> stored_template;
  bool                  verify_success  = false;
  int                   matched_id      = 0;
  template_ids_list.clear();
  DatabaseController::active_db_handle_->getAllTemplateIds(template_ids_list,
                                                           nonUser);
  // assess against stored
  for(int i = 0; i < template_ids_list.size(); i++)
  {
    //- clear container
    stored_template.first.clear();

    //- get next stored template
    DatabaseController::active_db_handle_->getTemplateById(template_ids_list.at(i),
                                                           stored_template,
                                                           nonUser);

    // verify
    if(newFingerprintScannerWindow->verify(add_recipient_template,stored_template.first))
    {
      verify_success  = true;
      matched_id      = stored_template.second;
      qDebug()<<"Match found!";
      break;
    }
  }
  if(verify_success)
  {
    RecipientsDetails recipient;
    DatabaseController::active_db_handle_->getRecipientById(matched_id,recipient);
    //- display no error
    ui->rs_lbl_info_error->setText(errorMsg(QString("Fingerprint already enrolled for - %1 %2!")
                                            .arg(recipient.name)
                                            .arg(recipient.surname)));

    add_recipient_template.clear();
  }
  else
  {
    ui->rs_lbl_info_error->setText(successMsg("Fingerprint Captured!"));
  }

}

void Recipients::on_rs_pb_learner_save_clicked()
{

  if(currentAdminAction == AddUser && addRecipient())
  {
    //- clear screen if all went well
    this->clearScreen();
    // populate desticts
    // get all regions
    current_regions_list.clear();
    DatabaseController::active_db_handle_->getAllRecipientsRegions(current_regions_list);

    // populate desticts
    for(int i = 0; i < current_regions_list.size(); i++)
      ui->rs_rgn_and_ln_list->addItem(current_regions_list.at(i).district);

     ui->rs_lbl_info_error->setText(successMsg("Recipient Added Successfully!"));
  }
  else if(currentAdminAction == EditUser && editRecipient())
  {
    //- clear screen if all went well
    this->clearScreen();
    // populate recipients
    QString temp_str;
    // get all recipients
    recipients_list.clear();
    DatabaseController::active_db_handle_->getAllRecipients(recipients_list);

    for(int i = 0; i < recipients_list.size(); i++)
    {
      DatabaseController::active_db_handle_->getSchoolNameById(temp_str, recipients_list.at(i).school_id);
      ui->rs_rgn_and_ln_list->addItem(recipients_list.at(i).name + " " + recipients_list.at(i).surname +
                                      " | " + temp_str);
      temp_str = "";
    }
    ui->rs_lbl_info_error->setText(successMsg("Recipient Editted Successfully!"));
  }
}

/*!
 * \brief Recipients::clearScreen
 */
void Recipients::clearScreen()
{
  // clear contents
  ui->rs_cmBx_district->clear();
  ui->rs_cmBx_school->clear();
  ui->rs_cmBx_type->setCurrentIndex(0);
  ui->rs_dateTimeEdit_dob->clear();
  ui->rs_le_address->clear();
  ui->rs_le_contact->clear();
  ui->rs_le_id->clear();
  ui->rs_le_name->clear();
  ui->rs_le_surname->clear();
  ui->rs_rgn_and_ln_list->clear();
  ui->rs_lbl_info_error->clear();
  ui->rs_lbl_fp_capture_status->clear();
  recipients_list.clear();
  school_list.clear();
  recipients_list_index = 0;
  current_regions_list.clear();
  add_recipient_template = NULL;

  ui->rs_tb_biometric_capture->setEnabled(true);
  ui->rs_cb_no_fingerprint->setChecked(false);

}

void Recipients::on_rs_pb_learner_delete_clicked()
{
  if(currentAdminAction == DeleteUser && removeRecipient())
  {
    QString temp_str;
    //- clear screen if all went well
    this->clearScreen();
    // set widgets
    ui->rs_pb_learner_save->setVisible(false);
    ui->rs_pb_learner_cancel->setVisible(true);
    ui->rs_pb_learner_delete->setVisible(true);
    ui->rs_pb_learner_search->setVisible(false);
    ui->rs_tb_biometric_capture->setEnabled(false);

    // get all recipients
    recipients_list.clear();
    DatabaseController::active_db_handle_->getAllRecipients(recipients_list);

    // populate desticts
    for(int i = 0; i < recipients_list.size(); i++){
      DatabaseController::active_db_handle_->getSchoolNameById(temp_str, recipients_list.at(i).school_id);
      ui->rs_rgn_and_ln_list->addItem(recipients_list.at(i).name + " " + recipients_list.at(i).surname +
                                      " | " + temp_str);
      temp_str = "";
    }

    ui->rs_lbl_info_error->setText(successMsg("Recipient Removed Successfully!"));
  }
}

void Recipients::on_rs_pb_learner_cancel_clicked()
{
  // close without saving
  this->clearScreen();
  this->close();
}


// -------------------------------------
// GROUP - Window
// -------------------------------------

// Note Group does not have a search button, reason being the
// group boxes will act as search btns as user types

void Recipients::on_rs_pb_grp_save_clicked()
{
  if(currentAdminAction == AddUser && addRecipient())
  {
    //- clear screen if all went well
    this->clearScreen();
    // populate desticts
    current_regions_list.clear();
    for(int i = 0; i < current_regions_list.size(); i++)
      ui->rs_rgn_and_ln_list->addItem(current_regions_list.at(i).district);

     ui->rs_lbl_info_error->setText(successMsg("Recipient Added Successfully!"));
  }
  else if(currentAdminAction == EditUser && editRecipient())
  {
    //- clear screen if all went well
    this->clearScreen();
    ui->rs_lbl_info_error->setText(successMsg("Recipient Editted Successfully!"));

    // populate recipients
    QString temp_str;
    for(int i = 0; i < recipients_list.size(); i++){
      DatabaseController::active_db_handle_->getSchoolNameById(temp_str, recipients_list.at(i).school_id);
      ui->rs_rgn_and_ln_list->addItem(recipients_list.at(i).name + " " + recipients_list.at(i).surname +
                                      " | " + temp_str);
      temp_str = "";
    }
  }
}

void Recipients::on_rs_pb_grp_delete_clicked()
{
  // remove group
}

void Recipients::on_rs_pb_grp_cancel_clicked()
{
  // close without saving
  this->clearScreen();
  this->close();
}

void Recipients::on_rs_rgn_and_ln_list_clicked(const QModelIndex &index)
{
  int ind = index.row();
  ui->rs_lbl_info_error->clear();
  ui->rs_lbl_fp_capture_status->clear();

  if(currentAdminAction == AddUser){

      // get all schools related to region/destrict
      school_list.clear();

      DatabaseController::active_db_handle_->getSchoolsByDistricId(school_list,index.row()+1);

      // populate the schools list
      ui->rs_cmBx_school->clear();
      for(int i = 0; i < school_list.size(); i++)
      ui->rs_cmBx_school->addItem(school_list.at(i).first);

      // populate the distric with the chosen district
      ui->rs_cmBx_district->clear();
      ui->rs_cmBx_district->addItem(current_regions_list.at(index.row()).district);

    }
    else if(currentAdminAction == DeleteUser || currentAdminAction == EditUser){

      QString temp_str;

      SystemUser::current_recipient_details = recipients_list.at(ind);

      ui->rs_le_address->setText(recipients_list.at(ind).address);
      ui->rs_le_contact->setText(recipients_list.at(ind).contact_number);
      ui->rs_le_id->setText(recipients_list.at(ind).id_number);
      ui->rs_le_name->setText(recipients_list.at(ind).name);
      ui->rs_le_surname->setText(recipients_list.at(ind).surname);
      ui->rs_dateTimeEdit_dob->setDate(recipients_list.at(ind).birth_date);
      ui->rs_cmBx_gender->setCurrentText(recipients_list.at(ind).gender);
      ui->rs_cmBx_type->setCurrentText(getRecipientTypeToString(recipients_list.at(ind).recipient_type));

      DatabaseController::active_db_handle_->getDistricNameById(temp_str,recipients_list.at(ind).region_id);
      ui->rs_cmBx_district->clear();
      ui->rs_cmBx_district->addItem(temp_str);
      DatabaseController::active_db_handle_->getSchoolNameById(temp_str,recipients_list.at(ind).school_id);
      ui->rs_cmBx_school->clear();
      ui->rs_cmBx_school->addItem(temp_str);
    }
}


void Recipients::on_rs_cb_no_fingerprint_clicked()
{

  ui->rs_tb_biometric_capture->setIcon(QIcon(":/Company/Logo/FingerScan.png"));
  ui->rs_tb_biometric_capture->setIconSize(ui->rs_tb_biometric_capture->size());

  ui->rs_lbl_fp_capture_status->clear();
  ui->rs_lbl_info_error->clear();

  if(ui->rs_cb_no_fingerprint->isChecked())
  {
    ui->rs_tb_biometric_capture->setEnabled(false);
  }
  else
  {
    ui->rs_tb_biometric_capture->setEnabled(true);
  }
}
