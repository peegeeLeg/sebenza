#-------------------------------------------------
#
# Project created by QtCreator 2018-08-14T18:46:18
#
#-------------------------------------------------

QT += core gui sql printsupport
QT += core network sql
QT += axcontainer
QT += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Sebenza
TEMPLATE = app

 INCLUDEPATH += "biomini"

SOURCES += main.cpp\
        homescreen.cpp \
    login_screen.cpp \
    database_controller.cpp \
    database.cpp \
    useradministration.cpp \
    system_user.cpp \
    SystemShare.cpp \
    recipients.cpp \
    fingerprintscanner.cpp \
    simplecrypt.cpp \
    identification_screen.cpp \
    distribution_activity_report.cpp \
    report_date_picker.cpp \
    excel_export_helper.cpp \
    import_csv.cpp \
    stock_manager_screen.cpp

HEADERS  += homescreen.h \
    login_screen.h \
    SystemShare.h \
    constants.h \
    database_controller.h \
    database.h \
    useradministration.h \
    system_user.h \
    recipients.h \
    fingerprintscanner.h \
    simplecrypt.h \
    biomini/UFDatabase.h \
    biomini/UFExtractor.h \
    biomini/UFMatcher.h \
    biomini/UFScanner.h \
    identification_screen.h \
    distribution_activity_report.h \
    report_date_picker.h \
    excel_export_helper.h \
    import_csv.h \
    stock_manager_screen.h

FORMS    += homescreen.ui \
    login_screen.ui \
    useradministration.ui \
    recipients.ui \
    fingerprintscanner.ui \
    identification_screen.ui \
    distribution_activity_report.ui \
    report_date_picker.ui \
    import_csv.ui \
    stock_manager_screen.ui

RESOURCES += \
    Kumaka_Resource.qrc
  # - Libraries
  LIBS += "UFDatabase.dll"\
          "UFExtractor.dll"\
          "UFMatcher.dll"\
          "UFScanner.dll"
