#ifndef RECIPIENTS_H
#define RECIPIENTS_H

#include <QDialog>
#include <SystemShare.h>
#include <database_controller.h>
#include <fingerprintscanner.h>

// Screens
#include <identification_screen.h>

namespace Ui {
class Recipients;
}

class Recipients : public QDialog
{
  Q_OBJECT

public:
  explicit Recipients(QWidget *parent = 0);
  ~Recipients();

  /*!
   * \brief setTypeScreen
   * \param action
   * \param recipientCatergory
   */
  void setTypeScreen(adminAction    action,
                     recipientType  recipientCatergory);
  /*!
   * \brief Recipients::addRecipient
   * \return true if recipient added successfully
   *         false if recipient failed to be added
   */
  bool addRecipient();

  /*!
   * \brief removeRecipient
   * \return true if recipient removed successfully
   *         false if recipient failed to be removed
   */
  bool removeRecipient();

  /*!
   * \brief Recipients::editRecipient
   * \return true if recipient editted successfully
   *         false if recipient failed to be editted
   */
  bool editRecipient();

  /*!
   * \brief Recipients::deleteRecipient
   * \return true if recipient deleted successfully
   *         false if recipient failed to be deleted
   */
  bool deleteRecipient();

  void getAllRecipients(QList<RecipientsDetails> &recipient_list);

  bool addStaff();

  /*!
   * \brief removeStaff
   * \return true if Staff removed successfully
   *         false if Staff failed to be removed
   */
  bool removeStaff();

  /*!
   * \brief Recipients::editStaff
   * \return true if Staff editted successfully
   *         false if Staff failed to be editted
   */
  bool editStaff();

  /*!
   * \brief Recipients::deleteStaff
   * \return true if Staff deleted successfully
   *         false if Staff failed to be deleted
   */
  bool deleteStafft();

  void getAllStaff(QList<StaffDetails> &Staff_list);



  void clearScreen();


  /*!
   * \brief setWidgets
   * \param enable
   */
  void setWidgets(bool enable);

private slots:

  void on_rs_pb_learner_seach_clicked();

//  void on_rs_tb_biometric_capture_clicked();

  void on_rs_pb_learner_save_clicked();

  void on_rs_pb_learner_delete_clicked();

  void on_rs_pb_learner_cancel_clicked();

  void on_rs_pb_grp_save_clicked();

  void on_rs_pb_grp_delete_clicked();

  void on_rs_pb_grp_cancel_clicked();

  void on_rs_rgn_and_ln_list_clicked(const QModelIndex &index);

  void on_rs_tb_biometric_capture_pressed();

  void on_rs_cb_no_fingerprint_clicked();

private:
  Ui::Recipients *ui;

  adminAction currentAdminAction;

  QList<RecipientsDetails>      recipients_list;
  QList< QPair<QString, int> >  school_list;
  int                           recipients_list_index;
  QList<RegionDetails>          current_regions_list;
  QByteArray                    add_recipient_template;
  int                           template_quality;

  // fingerprint scanner window
  FingerprintScanner* newFingerprintScannerWindow;

};

#endif // RECIPIENTS_H
