#ifndef STOCK_MANAGER_SCREEN_H
#define STOCK_MANAGER_SCREEN_H

#include <QDialog>

namespace Ui {
class stock_manager_screen;
}

class StockManagerScreen : public QDialog
{
  Q_OBJECT

public:
  explicit StockManagerScreen(QWidget *parent = 0);
  ~StockManagerScreen();

  void clearScreen();

  void reloadCount();

private slots:
  void on_sm_pb_save_clicked();

  void on_sm_pb_cancel_clicked();

private:
  Ui::stock_manager_screen *ui;
};

#endif // STOCK_MANAGER_SCREEN_H
