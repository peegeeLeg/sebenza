#include "distribution_activity_report.h"
#include "ui_distribution_activity_report.h"

#include <QPdfWriter>
#include <QTextDocument>
#include <QPainter>
#include <QDesktopServices>
#include <QUrl>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrintEngine>

DistributionActivityReport::DistributionActivityReport(QDate t_from,
                                                       QDate t_to,
                                                       QWidget *parent) :
  QDialog(parent),
  ui(new Ui::DistributionActivityReport)
{

  ui->setupUi(this);

  //  this->setWindowFlags(Qt::WindowStaysOnTopHint);

  ui->commandLinkButton->setVisible(true);

  // setup Screen
  //ui->dar_lbl_logo->setPixmap(QPixmap::fromImage(QImage((":/Company/Logo/small_logo.png"))));

  from_date = t_from;
  to_date   = t_to;

  ui->dar_tv_report_table->clear();

  if(SystemUser::Instance()->current_report_selected      == DistributionDetailsReport)
    generateDistributionDetailsReport();
  else if(SystemUser::Instance()->current_report_selected == DistributionSummaryReport)
    generateDistributionSummaryReport();
  else if(SystemUser::Instance()->current_report_selected == SystemUserListReport)
    generateSystemUsersListReport();
  else if(SystemUser::Instance()->current_report_selected == RegionDeliveriesReport)
      generateRegionDeliveriesReport();
  else if(SystemUser::Instance()->current_report_selected == SchoolDeliveriesReport)
      generateSchoolDeliveriesReport();


}

DistributionActivityReport::~DistributionActivityReport()
{
  delete ui;
}

void DistributionActivityReport::generateDistributionDetailsReport()
{
  current_report_name = "DistributionDetailsReport.pdf";
  current_html_string = "<div align=left><img src=\"kumaka-img3.png\" height=\"100\" width=\"442\" >";

  current_html_string += "</div> <div align=right><h2><b> Distribution Details Report </b></h2></div>";

  current_html_string += "<div align=left>";
  current_html_string +="<b>Date Range:</b> ";

  current_html_string += from_date.toString("dd-MM-yyyy").toStdString().c_str();

  current_html_string += " to ";
  current_html_string += to_date.toString("dd-MM-yyyy").toStdString().c_str();
  current_html_string += " <br><b>Generated on:</b> ";
  current_html_string += QDateTime::currentDateTime().toString("dd-MM-yyyy").toStdString().c_str();
  current_html_string += "</div>";

  current_html_string +=  "<br><br><table style=\"font-size: 8x;\" cellpadding=\"5\" "
                          "cellspacing=\"5\" style=\"width:100%\" border=\"0.1\"><tr>"
                          "<th style=\"width:5%\"><b>#</b></th>"
                          "<th style=\"width:15%\"><b>Recipient</b></th>"
                          "<th style=\"width:25%\"><b>District</b></th>"
                          "<th style=\"width:25%\"><b>School</b></th>"
                          "<th style=\"width:5%\"><b>Accepted By</b></th>"
                          "<th style=\"width:15%\"><b>Issued By</b></th>"
                          "<th style=\"width:10%\"><b>Date Issued</b></th>"
                          "</tr>";

  // setUp report headers
  int cols = 6;

  ui->dar_tv_report_table->clear();
  ui->dar_tv_report_table->reset();

  ui->dar_tv_report_table->setColumnCount(cols);
  ui->dar_tv_report_table->setHorizontalHeaderItem( 0, new QTableWidgetItem(QString("Recipient")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 1, new QTableWidgetItem(QString("District")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 2, new QTableWidgetItem(QString("School")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 3, new QTableWidgetItem(QString("Accepted By")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 4, new QTableWidgetItem(QString("Issued By")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 5, new QTableWidgetItem(QString("Date Issued")) );

  ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);


  // get selected date data
  QList<DistributionActivityDetails> dist_activity_list;

  DatabaseController::active_db_handle_->getDistributionActivityDetailsByDate( from_date,
                                                                               to_date,
                                                                               dist_activity_list);


  // set report
  ui->dar_lbl_datetime->setText(QString("Date Range: %1 to %2")
                                .arg(from_date.toString("dd-MM-yyyy"))
                                .arg(to_date.toString("dd-MM-yyyy")));

  ui->dar_lbl_report_title->setText("Distribution Activity Details");
  ui->dar_lbl_other_details->setText(QString("Generated on: %1")
                                     .arg(QDateTime::currentDateTime().toString("dd-MM-yyyy")));

  ui->dar_tv_report_table->setRowCount(dist_activity_list.size());
  ui->dar_tv_report_table->scrollBarWidgets(Qt::AlignLeft);
  ui->dar_tv_report_table->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QStringList       colValues;
  RecipientsDetails recipient;
  UserDetails       user;
  QString           temp_string;


  //populate table
  //Recipient | School | District | Accepted by | Issuer | date
  for(int i = 0; i < dist_activity_list.size(); i++)
  {
    //recipient
    DatabaseController::active_db_handle_->getRecipientById(dist_activity_list.at(i).recipient_id,recipient);
    colValues.append(QString("%1 %2").arg(recipient.name).arg(recipient.surname));

    //district
    DatabaseController::active_db_handle_->getDistricNameById(temp_string,recipient.region_id);
    colValues.append(temp_string);

    //school
    DatabaseController::active_db_handle_->getSchoolNameById(temp_string,recipient.school_id);
    colValues.append(temp_string);

    //accepted by
    colValues.append(getPackReceiverToString(dist_activity_list.at(i).accepted_by));

    //issuer
    DatabaseController::active_db_handle_->getUserDetailsById(user, dist_activity_list.at(i).user_id);
    colValues.append(QString("%1 %2").arg(user.name).arg(user.surname));
    //date
    colValues.append(dist_activity_list.at(i).date.toString());

    for(int col = 0; col < cols; col++)
    {
      ui->dar_tv_report_table->setItem(i,col,new QTableWidgetItem(colValues.at(col)));
    }
    ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    current_html_string +=  "<tr><td>";
    current_html_string +=  QString::number(i+1).toStdString().c_str();
    current_html_string +=  "</td>";
    current_html_string +=  "<td>";
    current_html_string +=  colValues.at(0);
    current_html_string +=  "</td>";
    current_html_string +=  "<td>";
    current_html_string +=  colValues.at(1);
    current_html_string +=  "</td>";
    current_html_string +=  "<td>";
    current_html_string +=  colValues.at(2);
    current_html_string +=  "</td>";
    current_html_string +=  "<td>";
    current_html_string +=  colValues.at(3);
    current_html_string +=  "</td>";
    current_html_string +=  "<td>";
    current_html_string +=  colValues.at(4);
    current_html_string +=  "</td>";
    current_html_string +=  "<td>";
    current_html_string +=  colValues.at(5);
    current_html_string +=  "</td></tr>";

    colValues.clear();
  }
  ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

  QTextDocument document;
  document.setHtml(current_html_string);

  QPrinter printer(QPrinter::PrinterResolution);
  printer.setOutputFormat(QPrinter::PdfFormat);
  printer.setPaperSize(QPrinter::A4);
  printer.setOutputFileName(current_report_name.toStdString().c_str());
  printer.setPageMargins(QMarginsF(1, 1, 1, 1));
  document.print(&printer);
}

void DistributionActivityReport::generateDistributionSummaryReport()
{
  //cont int variables
  const int cOne  = 1;
  const int cZero = 0;

  current_report_name = "DistributionSummaryReport.pdf";
  current_html_string = "<div align=left><img src=\"kumaka-img3.png\" height=\"100\" width=\"442\" >";

  current_html_string += "</div> <div align=right><h2><b> Distribution Summary Report </b></h2></div>";

  current_html_string += "<div align=left>";
  current_html_string +="<b>Date Range:</b> ";

  current_html_string += from_date.toString("dd-MM-yyyy").toStdString().c_str();

  current_html_string += " to ";
  current_html_string += to_date.toString("dd-MM-yyyy").toStdString().c_str();
  current_html_string += " <br><b>Generated on:</b> ";
  current_html_string += QDateTime::currentDateTime().toString("dd-MM-yyyy").toStdString().c_str();
  current_html_string += "</div>";

  current_html_string +=  "<br><br><table style=\"font-size: 8x;\" cellpadding=\"5\" "
                          "cellspacing=\"5\" style=\"width:100%\" border=\"0.1\"><tr>"
                          "<th style=\"width:5%\"><b>#</b></th>"
                          "<th style=\"width:30%\"><b>District</b></th>"
                          "<th style=\"width:30%\"><b>School</b></th>"
                          "<th align=\"center\" style=\"width:10%\"><b>Rcvd by SELF</b></th>"
                          "<th align=\"center\" style=\"width:10%\"><b>Rcvd by AUTHORISED</b></th>"
                          "<th align=\"center\" style=\"width:10%\"><b>Rcvd by ISSUER</b></th>"
                          "<th align=\"center\" style=\"width:10%\"><b>Recipients issued to</b></th>"
                          "<th align=\"center\" style=\"width:10%\"><b>packs</b></th>"
                          "</tr>";

  // setUp report headers
  int cols = 7;
  ui->dar_tv_report_table->clear();
  ui->dar_tv_report_table->reset();

  ui->dar_tv_report_table->setColumnCount(cols);
  ui->dar_tv_report_table->setHorizontalHeaderItem( 0, new QTableWidgetItem(QString("District")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 1, new QTableWidgetItem(QString("School")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 2, new QTableWidgetItem(QString("Rcvd by SELF")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 3, new QTableWidgetItem(QString("Rcvd by AUTHORISED")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 4, new QTableWidgetItem(QString("Rcvd by ISSUER")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 5, new QTableWidgetItem(QString("Recipients issued to")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 6, new QTableWidgetItem(QString("packs")) );

  ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

  // set report
  ui->dar_lbl_datetime->setText(QString("Date Range: %1 to %2")
                                .arg(from_date.toString("dd-MM-yyyy"))
                                .arg(to_date.toString("dd-MM-yyyy")));

  ui->dar_lbl_report_title->setText("Distribution Activity Details");
  ui->dar_lbl_other_details->setText(QString("Generated on: %1")
                                     .arg(QDateTime::currentDateTime().toString("dd-MM-yyyy")));


  QFont font;
  font.setBold(true);
  ui->dar_tv_report_table->setFont(font);
  int temp = 0;
  DatabaseController::active_db_handle_->getNumberOfSchools(temp);
  ui->dar_tv_report_table->setRowCount(temp);
  ui->dar_tv_report_table->scrollBarWidgets(Qt::AlignLeft);
  ui->dar_tv_report_table->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  int packs_rcvd_by_self_count    = 0;
  int packs_rcvd_by_auth_count    = 0;
  int packs_rcvd_by_issuer_count  = 0;
  int recipients_issued_for_count = 0;
  int packs_count                 = 0;

  QStringList                 colValues;
  QString                     temp_string;
  DistributionActivitySummary dist_activity;

  // get list of all regions and schools
  QList<RegionDetails>        region_list;
  QList< QPair<QString,int> > school_list;
  QList<int>                  recipient_id_list;

  DatabaseController::active_db_handle_->getAllRecipientsRegions(region_list);

  int count = 0;
  for(int i = 0; i < region_list.size(); i++)
  {
    school_list.clear();
    DatabaseController::active_db_handle_->getSchoolsByDistricId(school_list,region_list.at(i).region_id);

    //- get all recipients for each school
    for(int j = 0; j < school_list.size(); j++)
    {
      DatabaseController::active_db_handle_->getRecipientsIdsBySchool(school_list.at(j).second,recipient_id_list);

      //clear variable for reuse
      packs_rcvd_by_self_count    = 0;
      packs_rcvd_by_auth_count    = 0;
      packs_rcvd_by_issuer_count  = 0;
      recipients_issued_for_count = 0;
      packs_count                 = 0;

      //-- get the stats for each learner and add up
      for(int k = 0; k < recipient_id_list.size(); k++)
      {

        // get selected date data
        DatabaseController::active_db_handle_->getDistributionActivitySummaryByDate(from_date,
                                                                                    to_date,
                                                                                    recipient_id_list.at(k),
                                                                                    dist_activity);
        packs_rcvd_by_self_count    += dist_activity.packs_rcvd_by_self_count;
        packs_rcvd_by_auth_count    += dist_activity.packs_rcvd_by_authorised_count;
        packs_rcvd_by_issuer_count  += dist_activity.packs_rcvd_by_issuer_count;
        recipients_issued_for_count += ((dist_activity.packs_count > 0) ? cOne : cZero);
        packs_count                 += dist_activity.packs_count;
      }

      //-- populate line items
      //district
      DatabaseController::active_db_handle_->getDistricNameById(temp_string,region_list.at(i).region_id);
      colValues.append(temp_string);
      //school
      DatabaseController::active_db_handle_->getSchoolNameById(temp_string,school_list.at(j).second);
      colValues.append(temp_string);
      //# of packs rcvd by self
      colValues.append(QString::number(packs_rcvd_by_self_count));
      //# of packs rcvd by auth
      colValues.append(QString::number(packs_rcvd_by_auth_count));
      //# of packs rcvd by issuer
      colValues.append(QString::number(packs_rcvd_by_issuer_count));
      //# of recipients issued for
      colValues.append(QString::number(recipients_issued_for_count));
      //# of packs
      colValues.append(QString::number(packs_count));

      for(int col = 0; col < cols; col++)
      {
        ui->dar_tv_report_table->setItem(count,col,new QTableWidgetItem(colValues.at(col)));
      }
      ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

      current_html_string +=  "<tr><td style=\"width:5%\">";
      current_html_string +=  QString::number(++count).toStdString().c_str();
      current_html_string +=  "</td>";
      current_html_string +=  "<td style=\"width:30%\">";
      current_html_string +=  colValues.at(0);
      current_html_string +=  "</td>";
      current_html_string +=  "<td style=\"width:30%\">";
      current_html_string +=  colValues.at(1);
      current_html_string +=  "</td>";
      current_html_string +=  "<td align=\"center\" style=\"width:10%\">";
      current_html_string +=  colValues.at(2);
      current_html_string +=  "</td>";
      current_html_string +=  "<td align=\"center\" style=\"width:10%\" >";
      current_html_string +=  colValues.at(3);
      current_html_string +=  "</td>";
      current_html_string +=  "<td align=\"center\" style=\"width:10%\" >";
      current_html_string +=  colValues.at(4);
      current_html_string +=  "</td>";
      current_html_string +=  "<td align=\"center\" style=\"width:10%\" >";
      current_html_string +=  colValues.at(5);
      current_html_string +=  "<td align=\"center\" style=\"width:10%\" >";
      current_html_string +=  colValues.at(6);
      current_html_string +=  "</td></tr>";
      colValues.clear();
    }
  }

  ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  QTextDocument document;
  document.setHtml(current_html_string);

  QPrinter printer(QPrinter::PrinterResolution);
  printer.setOutputFormat(QPrinter::PdfFormat);
  printer.setPaperSize(QPrinter::A4);
  printer.setOutputFileName(current_report_name.toStdString().c_str());
  printer.setPageMargins(QMarginsF(1, 1, 1, 1));
  document.print(&printer);
}

void DistributionActivityReport::generateSystemUsersListReport()
{
  current_report_name = "SystemUsersListReport.pdf";
  current_html_string = "<div align=left><img src=\"kumaka-img3.png\" height=\"100\" width=\"442\" >";

  current_html_string += "</div> <div align=right><h2><b> System Users List Report </b></h2></div>";

  current_html_string += "<div align=left>";

//  current_html_string +="<b>Date Range:</b> ";
//  current_html_string += from_date.toString("dd-MM-yyyy").toStdString().c_str();
//  current_html_string += " to ";
//  current_html_string += to_date.toString("dd-MM-yyyy").toStdString().c_str();

  current_html_string += " <br><b>Generated on:</b> ";
  current_html_string += QDateTime::currentDateTime().toString("dd-MM-yyyy").toStdString().c_str();
  current_html_string += "</div>";

  current_html_string +=  "<br><br><table style=\"font-size: 8x;\" cellpadding=\"5\" "
                          "cellspacing=\"5\" style=\"width:100%\" border=\"0.1\"><tr>"
                          "<th style=\"width:5%\"><b>#</b></th>"
                          "<th style=\"width:15%\"><b>Name</b></th>"
                          "<th style=\"width:15%\"><b>Surname</b></th>"
                          "<th style=\"width:15%\"><b>Type</b></th>"
                          "<th align=\"center\" style=\"width:15%\"><b>Can Add/Edit Users</b></th>"
                          "<th align=\"center\" style=\"width:10%\"><b>Can Add/Edit Recipients</b></th>"
                          "<th align=\"center\" style=\"width:10%\"><b>Can View Reports</b></th>"
                          "<th align=\"center\" style=\"width:15%\"><b>Can Import Schools</b></th>"
                          "<th align=\"center\" style=\"width:15%\"><b>Can View Audits</b></th>"
                          "<th align=\"center\" style=\"width:15%\"><b>Can View Stock</b></th>"
                          "</tr>";

  // setUp report headers
  int cols = 9;
  ui->dar_tv_report_table->clear();
  ui->dar_tv_report_table->reset();

  ui->dar_tv_report_table->setColumnCount(cols);
  ui->dar_tv_report_table->setHorizontalHeaderItem( 0, new QTableWidgetItem(QString("Name")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 1, new QTableWidgetItem(QString("Surname")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 2, new QTableWidgetItem(QString("Type")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 3, new QTableWidgetItem(QString("Can Add/Edit Users")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 4, new QTableWidgetItem(QString("Can Add/Edit Recipients")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 5, new QTableWidgetItem(QString("Can View Reports")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 6, new QTableWidgetItem(QString("Can Import Schools")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 7, new QTableWidgetItem(QString("Can View Audits")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 8, new QTableWidgetItem(QString("Can View Stock")) );

  ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

  // set report
//  ui->dar_lbl_datetime->setText(QString("Date Range: %1 to %2")
//                                .arg(from_date.toString("dd-MM-yyyy"))
//                                .arg(to_date.toString("dd-MM-yyyy")));

  ui->dar_lbl_report_title->setText("System Users List");
  ui->dar_lbl_other_details->setText(QString("Generated on: %1")
                                     .arg(QDateTime::currentDateTime().toString("dd-MM-yyyy")));


  QFont font;
  font.setBold(true);
  ui->dar_tv_report_table->setFont(font);
  int temp = 0;
  DatabaseController::active_db_handle_->getNumberOfSchools(temp);
  ui->dar_tv_report_table->setRowCount(temp);
  ui->dar_tv_report_table->scrollBarWidgets(Qt::AlignLeft);
  ui->dar_tv_report_table->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QStringList                 colValues;

  // get list of all system users
  QList<QPair<UserDetails ,int> > users_list;
  DatabaseController::active_db_handle_->getAllSystemUsers(users_list);

  int count = 0;
  for(int i = 0; i < users_list.size(); i++)
  {
    // -- populate line items
    // -- name
    colValues.append(users_list.at(i).first.name);
    // -- surname
    colValues.append(users_list.at(i).first.surname);
    // -- type
    if(users_list.at(i).first.user_type == Admin )
      colValues.append("Admin");
    else if(users_list.at(i).first.user_type == SuperAdmin )
      colValues.append("Super Admin");
    else if(users_list.at(i).first.user_type == Distributor )
      colValues.append("Distributor");
    else
      colValues.append("Unknown");

    // -- can add/edit users
    if(users_list.at(i).first.user_type == Admin ||
       users_list.at(i).first.user_type == SuperAdmin)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can add/edit recipients
    if(users_list.at(i).first.user_type == Admin      ||
       users_list.at(i).first.user_type == SuperAdmin ||
       users_list.at(i).first.user_type == Distributor)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can view reports
    if(users_list.at(i).first.user_type == Admin      ||
       users_list.at(i).first.user_type == SuperAdmin)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can add/edit recipients
    if(users_list.at(i).first.user_type == Admin  ||
       users_list.at(i).first.user_type == Distributor)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can import schools
    if(users_list.at(i).first.user_type == Admin      ||
       users_list.at(i).first.user_type == SuperAdmin ||
       users_list.at(i).first.user_type == Distributor)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can view audits
    if(users_list.at(i).first.user_type == SuperAdmin)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can view stock
    if(users_list.at(i).first.user_type == Admin      ||
       users_list.at(i).first.user_type == SuperAdmin ||
       users_list.at(i).first.user_type == Distributor)
      colValues.append("Yes");
    else
      colValues.append("No");

    for(int col = 0; col < cols; col++)
    {
      ui->dar_tv_report_table->setItem(count,col,new QTableWidgetItem(colValues.at(col)));
    }
    ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    current_html_string +=  "<tr><td style=\"width:5%\">";
    current_html_string +=  QString::number(++count).toStdString().c_str();
    current_html_string +=  "</td>";
    current_html_string +=  "<td style=\"width:15%\">";
    current_html_string +=  colValues.at(0);
    current_html_string +=  "</td>";
    current_html_string +=  "<td style=\"width:15%\">";
    current_html_string +=  colValues.at(1);
    current_html_string +=  "</td>";
    current_html_string +=  "<td style=\"width:15%\">";
    current_html_string +=  colValues.at(2);
    current_html_string +=  "</td>";
    current_html_string +=  "<td align=\"center\" style=\"width:15%\">";
    current_html_string +=  colValues.at(3);
    current_html_string +=  "</td>";
    current_html_string +=  "<td align=\"center\" style=\"width:15%\" >";
    current_html_string +=  colValues.at(4);
    current_html_string +=  "</td>";
    current_html_string +=  "<td align=\"center\" style=\"width:15%\" >";
    current_html_string +=  colValues.at(5);
    current_html_string +=  "</td>";
    current_html_string +=  "<td align=\"center\" style=\"width:15%\" >";
    current_html_string +=  colValues.at(6);
    current_html_string +=  "<td align=\"center\" style=\"width:15%\" >";
    current_html_string +=  colValues.at(7);
    current_html_string +=  "<td align=\"center\" style=\"width:15%\" >";
    current_html_string +=  colValues.at(8);
    current_html_string +=  "</td></tr>";
    colValues.clear();
  }

  ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  QTextDocument document;
  document.setHtml(current_html_string);

  QPrinter printer(QPrinter::PrinterResolution);
  printer.setOutputFormat(QPrinter::PdfFormat);
  printer.setPaperSize(QPrinter::A4);
  printer.setOutputFileName(current_report_name.toStdString().c_str());
  printer.setPageMargins(QMarginsF(1, 1, 1, 1));
  document.print(&printer);
}

void DistributionActivityReport::generateRecipientsListReport()
{
  current_report_name = "SystemUsersListReport.pdf";
  current_html_string = "<div align=left><img src=\"kumaka-img3.png\" height=\"100\" width=\"442\" >";

  current_html_string += "</div> <div align=right><h2><b> System Users List Report </b></h2></div>";

  current_html_string += "<div align=left>";

  current_html_string += " <br><b>Generated on:</b> ";
  current_html_string += QDateTime::currentDateTime().toString("dd-MM-yyyy").toStdString().c_str();
  current_html_string += "</div>";

  current_html_string +=  "<br><br><table style=\"font-size: 8x;\" cellpadding=\"5\" "
                          "cellspacing=\"5\" style=\"width:100%\" border=\"0.1\"><tr>"
                          "<th style=\"width:5%\"><b>#</b></th>"
                          "<th style=\"width:15%\"><b>Name</b></th>"
                          "<th style=\"width:15%\"><b>Surname</b></th>"
                          "<th style=\"width:15%\"><b>Type</b></th>"
                          "<th align=\"center\" style=\"width:15%\"><b>Can Add/Edit Users</b></th>"
                          "<th align=\"center\" style=\"width:10%\"><b>Can Add/Edit Recipients</b></th>"
                          "<th align=\"center\" style=\"width:10%\"><b>Can View Reports</b></th>"
                          "<th align=\"center\" style=\"width:15%\"><b>Can Import Schools</b></th>"
                          "<th align=\"center\" style=\"width:15%\"><b>Can View Audits</b></th>"
                          "<th align=\"center\" style=\"width:15%\"><b>Can View Stock</b></th>"
                          "</tr>";

  // setUp report headers
  int cols = 9;
  ui->dar_tv_report_table->clear();
  ui->dar_tv_report_table->reset();

  ui->dar_tv_report_table->setColumnCount(cols);
  ui->dar_tv_report_table->setHorizontalHeaderItem( 0, new QTableWidgetItem(QString("Name")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 1, new QTableWidgetItem(QString("Surname")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 2, new QTableWidgetItem(QString("Type")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 3, new QTableWidgetItem(QString("Can Add/Edit Users")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 4, new QTableWidgetItem(QString("Can Add/Edit Recipients")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 5, new QTableWidgetItem(QString("Can View Reports")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 6, new QTableWidgetItem(QString("Can Import Schools")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 7, new QTableWidgetItem(QString("Can View Audits")) );
  ui->dar_tv_report_table->setHorizontalHeaderItem( 8, new QTableWidgetItem(QString("Can View Stock")) );

  ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

  // set report
//  ui->dar_lbl_datetime->setText(QString("Date Range: %1 to %2")
//                                .arg(from_date.toString("dd-MM-yyyy"))
//                                .arg(to_date.toString("dd-MM-yyyy")));

  ui->dar_lbl_report_title->setText("System Users List");
  ui->dar_lbl_other_details->setText(QString("Generated on: %1")
                                     .arg(QDateTime::currentDateTime().toString("dd-MM-yyyy")));

  int temp = 0;
  DatabaseController::active_db_handle_->getNumberOfSchools(temp);
  ui->dar_tv_report_table->setRowCount(temp);
  ui->dar_tv_report_table->scrollBarWidgets(Qt::AlignLeft);
  ui->dar_tv_report_table->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QStringList                 colValues;

  // get list of all system users
  QList<QPair<UserDetails ,int> > users_list;
  DatabaseController::active_db_handle_->getAllSystemUsers(users_list);

  int count = 0;
  for(int i = 0; i < users_list.size(); i++)
  {
    // -- populate line items
    // -- name
    colValues.append(users_list.at(i).first.name);
    // -- surname
    colValues.append(users_list.at(i).first.surname);
    // -- type
    if(users_list.at(i).first.user_type == Admin )
      colValues.append("Admin");
    else if(users_list.at(i).first.user_type == SuperAdmin )
      colValues.append("Super Admin");
    else if(users_list.at(i).first.user_type == Distributor )
      colValues.append("Distributor");
    else
      colValues.append("Unknown");

    // -- can add/edit users
    if(users_list.at(i).first.user_type == Admin ||
       users_list.at(i).first.user_type == SuperAdmin)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can add/edit recipients
    if(users_list.at(i).first.user_type == Admin      ||
       users_list.at(i).first.user_type == SuperAdmin ||
       users_list.at(i).first.user_type == Distributor)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can view reports
    if(users_list.at(i).first.user_type == Admin      ||
       users_list.at(i).first.user_type == SuperAdmin)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can add/edit recipients
    if(users_list.at(i).first.user_type == Admin  ||
       users_list.at(i).first.user_type == Distributor)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can import schools
    if(users_list.at(i).first.user_type == Admin      ||
       users_list.at(i).first.user_type == SuperAdmin ||
       users_list.at(i).first.user_type == Distributor)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can view audits
    if(users_list.at(i).first.user_type == SuperAdmin)
      colValues.append("Yes");
    else
      colValues.append("No");

    // -- can view stock
    if(users_list.at(i).first.user_type == Admin      ||
       users_list.at(i).first.user_type == SuperAdmin ||
       users_list.at(i).first.user_type == Distributor)
      colValues.append("Yes");
    else
      colValues.append("No");

    for(int col = 0; col < cols; col++)
    {
      ui->dar_tv_report_table->setItem(count,col,new QTableWidgetItem(colValues.at(col)));
    }
    ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    current_html_string +=  "<tr><td style=\"width:5%\">";
    current_html_string +=  QString::number(++count).toStdString().c_str();
    current_html_string +=  "</td>";
    current_html_string +=  "<td style=\"width:15%\">";
    current_html_string +=  colValues.at(0);
    current_html_string +=  "</td>";
    current_html_string +=  "<td style=\"width:15%\">";
    current_html_string +=  colValues.at(1);
    current_html_string +=  "</td>";
    current_html_string +=  "<td style=\"width:15%\">";
    current_html_string +=  colValues.at(2);
    current_html_string +=  "</td>";
    current_html_string +=  "<td align=\"center\" style=\"width:15%\">";
    current_html_string +=  colValues.at(3);
    current_html_string +=  "</td>";
    current_html_string +=  "<td align=\"center\" style=\"width:15%\" >";
    current_html_string +=  colValues.at(4);
    current_html_string +=  "</td>";
    current_html_string +=  "<td align=\"center\" style=\"width:15%\" >";
    current_html_string +=  colValues.at(5);
    current_html_string +=  "</td>";
    current_html_string +=  "<td align=\"center\" style=\"width:15%\" >";
    current_html_string +=  colValues.at(6);
    current_html_string +=  "<td align=\"center\" style=\"width:15%\" >";
    current_html_string +=  colValues.at(7);
    current_html_string +=  "<td align=\"center\" style=\"width:15%\" >";
    current_html_string +=  colValues.at(8);
    current_html_string +=  "</td></tr>";
    colValues.clear();
  }

  ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  QTextDocument document;
  document.setHtml(current_html_string);

  QPrinter printer(QPrinter::PrinterResolution);
  printer.setOutputFormat(QPrinter::PdfFormat);
  printer.setPaperSize(QPrinter::A4);
  printer.setOutputFileName(current_report_name.toStdString().c_str());
  printer.setPageMargins(QMarginsF(1, 1, 1, 1));
  document.print(&printer);
}

void DistributionActivityReport::generateRegionDeliveriesReport()
{
    current_report_name = "RegionDeliveriesReport.pdf";
    current_html_string = "<div align=left><img src=\"kumaka-img3.png\" height=\"100\" width=\"442\" >";

    current_html_string += "</div> <div align=right><h2><b> Region Deliveries Report</b></h2></div>";

    current_html_string += "<div align=left>";
    current_html_string +="<b>Date Range:</b> ";

    current_html_string += from_date.toString("dd-MM-yyyy").toStdString().c_str();

    current_html_string += " to ";
    current_html_string += to_date.toString("dd-MM-yyyy").toStdString().c_str();
    current_html_string += " <br><b>Generated on:</b> ";
    current_html_string += QDateTime::currentDateTime().toString("dd-MM-yyyy").toStdString().c_str();
    current_html_string += "</div>";

    current_html_string +=  "<br><br><table style=\"font-size: 8x;\" cellpadding=\"5\" "
                            "cellspacing=\"5\" style=\"width:100%\" border=\"0.1\"><tr>"
                            "<th style=\"width:5%\"><b>#</b></th>"
                            "<th style=\"width:15%\"><b>Region</b></th>"
                            "<th style=\"width:25%\"><b>District</b></th>"
                            "<th style=\"width:10%\"><b>Number Of Deliveries</b></th>"
                            "</tr>";

    // setUp report headers
    int cols = 3;
    ui->dar_tv_report_table->setColumnCount(cols);
    ui->dar_tv_report_table->setHorizontalHeaderItem( 0, new QTableWidgetItem(QString("Region")) );
    ui->dar_tv_report_table->setHorizontalHeaderItem( 1, new QTableWidgetItem(QString("District")) );
    ui->dar_tv_report_table->setHorizontalHeaderItem( 2, new QTableWidgetItem(QString("Number Of Deliveries")) );

    // get selected date data
    QList<RegionDeliveries> region_del_list;

    DatabaseController::active_db_handle_->getRegionDeliveriesByDate( from_date,
                                                                      to_date,
                                                                      region_del_list);


    // set report
    ui->dar_lbl_datetime->setText(QString("Date Range: %1 to %2")
                                  .arg(from_date.toString("dd-MM-yyyy"))
                                  .arg(to_date.toString("dd-MM-yyyy")));

    ui->dar_lbl_report_title->setText("Region Deliveries Report");
    ui->dar_lbl_other_details->setText(QString("Generated on: %1")
                                       .arg(QDateTime::currentDateTime().toString("dd-MM-yyyy")));

    ui->dar_tv_report_table->setRowCount(region_del_list.size());
    ui->dar_tv_report_table->scrollBarWidgets(Qt::AlignLeft);

    QStringList       colValues;

    //populate table
    //Region | District | Count
    for(int i = 0; i < region_del_list.size(); i++)
    {
      //region
      colValues.append(QString("%1").arg(region_del_list.at(i).region));

      //district
      colValues.append(region_del_list.at(i).district);

      //count
      colValues.append(QString("%1").arg(region_del_list.at(i).count));

      for(int col = 0; col < cols; col++)
      {
        ui->dar_tv_report_table->setItem(i,col,new QTableWidgetItem(colValues.at(col)));
      }

      current_html_string +=  "<tr><td>";
      current_html_string +=  QString::number(i+1).toStdString().c_str();
      current_html_string +=  "</td>";
      current_html_string +=  "<td>";
      current_html_string +=  colValues.at(0);
      current_html_string +=  "</td>";
      current_html_string +=  "<td>";
      current_html_string +=  colValues.at(1);
      current_html_string +=  "</td>";
      current_html_string +=  "<td>";
      current_html_string +=  colValues.at(2);
      current_html_string +=  "</td></tr>";

      colValues.clear();
    }
    ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    QTextDocument document;
    document.setHtml(current_html_string);

    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setOutputFileName(current_report_name.toStdString().c_str());
    printer.setPageMargins(QMarginsF(1, 1, 1, 1));
    document.print(&printer);

}


void DistributionActivityReport::generateSchoolDeliveriesReport()
{
    current_report_name = "SchoolDeliveriesReport.pdf";
    current_html_string = "<div align=left><img src=\"kumaka-img3.png\" height=\"100\" width=\"442\" >";

    current_html_string += "</div> <div align=right><h2><b> School Deliveries Report</b></h2></div>";

    current_html_string += "<div align=left>";
    current_html_string +="<b>Date Range:</b> ";

    current_html_string += from_date.toString("dd-MM-yyyy").toStdString().c_str();

    current_html_string += " to ";
    current_html_string += to_date.toString("dd-MM-yyyy").toStdString().c_str();
    current_html_string += " <br><b>Generated on:</b> ";
    current_html_string += QDateTime::currentDateTime().toString("dd-MM-yyyy").toStdString().c_str();
    current_html_string += "</div>";

    current_html_string +=  "<br><br><table style=\"font-size: 8x;\" cellpadding=\"5\" "
                            "cellspacing=\"5\" style=\"width:100%\" border=\"0.1\"><tr>"
                            "<th style=\"width:5%\"><b>#</b></th>"
                            "<th style=\"width:15%\"><b>School</b></th>"
                            "<th style=\"width:10%\"><b>Number Of Deliveries</b></th>"
                            "</tr>";

    // setUp report headers
    int cols = 2;
    ui->dar_tv_report_table->setColumnCount(cols);
    ui->dar_tv_report_table->setHorizontalHeaderItem( 0, new QTableWidgetItem(QString("School")) );
    ui->dar_tv_report_table->setHorizontalHeaderItem( 1, new QTableWidgetItem(QString("Number Of Deliveries")) );

    // get selected date data
    QList<SchoolDeliveries> school_del_list;

    DatabaseController::active_db_handle_->getSchoolDeliveriesByDate( from_date,
                                                                      to_date,
                                                                      school_del_list);


    // set report
    ui->dar_lbl_datetime->setText(QString("Date Range: %1 to %2")
                                  .arg(from_date.toString("dd-MM-yyyy"))
                                  .arg(to_date.toString("dd-MM-yyyy")));

    ui->dar_lbl_report_title->setText("Region Deliveries Report");
    ui->dar_lbl_other_details->setText(QString("Generated on: %1")
                                       .arg(QDateTime::currentDateTime().toString("dd-MM-yyyy")));

    ui->dar_tv_report_table->setRowCount(school_del_list.size());
    ui->dar_tv_report_table->scrollBarWidgets(Qt::AlignLeft);

    QStringList       colValues;

    //populate table
    //School | Count
    for(int i = 0; i < school_del_list.size(); i++)
    {
      //school
      colValues.append(school_del_list.at(i).school);

      //count
      colValues.append(QString("%1").arg(school_del_list.at(i).count));

      for(int col = 0; col < cols; col++)
      {
        ui->dar_tv_report_table->setItem(i,col,new QTableWidgetItem(colValues.at(col)));
      }

      current_html_string +=  "<tr><td>";
      current_html_string +=  QString::number(i+1).toStdString().c_str();
      current_html_string +=  "</td>";
      current_html_string +=  "<td>";
      current_html_string +=  colValues.at(0);
      current_html_string +=  "</td>";
      current_html_string +=  "<td>";
      current_html_string +=  colValues.at(1);
      current_html_string +=  "</td></tr>";

      colValues.clear();
    }
    ui->dar_tv_report_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    QTextDocument document;
    document.setHtml(current_html_string);

    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setOutputFileName(current_report_name.toStdString().c_str());
    printer.setPageMargins(QMarginsF(1, 1, 1, 1));
    document.print(&printer);

}


void DistributionActivityReport::on_commandLinkButton_clicked()
{
  QDesktopServices desk;
  desk.openUrl(QUrl(current_report_name.toStdString().c_str()));
}
