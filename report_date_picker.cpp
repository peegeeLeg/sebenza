#include "report_date_picker.h"
#include "ui_report_date_picker.h"


ReportDatePicker::ReportDatePicker(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ReportDatePicker)
{
  ui->setupUi(this);
  this->setWindowFlags(Qt::WindowStaysOnTopHint);
  this->setWindowTitle("Report Date Picker");

}

ReportDatePicker::~ReportDatePicker()
{
  delete ui;
}

void ReportDatePicker::on_rdp_pb_select_clicked()
{

  t_from  = ui->rdp_de_from_date->selectedDate();
  t_to    = ui->rdp_de_to_date->selectedDate();

  // setup report screens
  newDistributionAReport  = new DistributionActivityReport(t_from,t_to);

  this->close();

  newDistributionAReport->setModal(true);
  newDistributionAReport->exec();
}

void ReportDatePicker::on_rdp_pb_cancel_clicked()
{
  this->close();
}
