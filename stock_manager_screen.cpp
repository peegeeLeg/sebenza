#include "stock_manager_screen.h"
#include "ui_stock_manager_screen.h"
#include "database_controller.h"

StockManagerScreen::StockManagerScreen(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::stock_manager_screen)
{
    ui->setupUi(this);
    // set window title
    this->setWindowTitle(QString("Stock Management"));

    // get current values
    this->reloadCount();
}

StockManagerScreen::~StockManagerScreen()
{
    delete ui;
}

void StockManagerScreen::clearScreen()
{
    // Variables
    const int cZero = 0;

    // Clear
    ui->sm_cmt_text_area->clear();
    ui->sm_lbl_msg->clear();
    ui->sm_spimBx_stock_item_count->setValue(cZero);
    ui->sm_spimBx_stock_limit_count->setValue(cZero);
    ui->sm_lbl_current_stock_limit_count->clear();
    ui->sm_lbl_current_limit_reached_email->clear();
}

void StockManagerScreen::reloadCount()
{
    // variable
    double cnt;
    double limit;
    QString alert_mail;

    this->clearScreen();
    // get and show current dignity count
    cnt = 0;
    DatabaseController::active_db_handle_->getDignityPackCounts(cnt);
    ui->sm_lbl_msg->setText(warningMsg(QString("Number of Dignity Packs in stock: %1").arg(cnt)));

    // get and show current limit count and email
    limit = 0;
    alert_mail.clear();

    DatabaseController::active_db_handle_->getDignityPackLimitDetails(limit,alert_mail);

    ui->sm_lbl_current_stock_limit_count->setText(normalMsg(QString("Current: %1").arg(limit)));
    ui->sm_lbl_current_limit_reached_email->setText(normalMsg(QString("Current: %1").arg(alert_mail)));
}

void StockManagerScreen::on_sm_pb_save_clicked()
{
    // const variables
    const int cZero       = 0;
    const int cMinusOne   = -1;

    // int variable
    int result;

    // get entered values
    QString entered_email = (QString)ui->sm_le_new_email->text();
    QString comment       = (QString)ui->sm_cmt_text_area->toPlainText();
    double  updated_count = (double)ui->sm_spimBx_stock_item_count->value();
    double  new_limit     = (double)ui->sm_spimBx_stock_limit_count->value();

    // Email validity check
    QRegExp mailREX("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
    mailREX.setCaseSensitivity(Qt::CaseInsensitive);
    mailREX.setPatternSyntax(QRegExp::RegExp);

    if(!entered_email.trimmed().isEmpty() && !mailREX.exactMatch(entered_email)){

        ui->sm_lbl_msg->setText(errorMsg(QString("Invalid Email Entered!")));

        return;
    }

    result = cZero;

    // check if changes made to settings
    if(!entered_email.trimmed().isEmpty() || new_limit > (double)cZero){

        // - update email and limit
        result = DatabaseController::active_db_handle_->updateStockSettings(new_limit,
                                                                            entered_email);
    }

    // evaluate result
    if(result != cZero){

        ui->sm_lbl_msg->setText(errorMsg(QString("Failed to add Stock settings!")));

        return;
    }

    // update dignity pack count
    if(updated_count > (double)cZero){

        // - update dignity pack
        result = DatabaseController::active_db_handle_->updateDignityPacks(updated_count,
                                                                           comment,
                                                                           ADD);
    }

    // get current values
    this->reloadCount();

    if(result == cZero){
        ui->sm_lbl_msg->setText(successMsg(QString("Successfully added items!")));
    }
    else if(result == cMinusOne){
        ui->sm_lbl_msg->setText(errorMsg(QString("Database issue, failed to add items!")));
    }
    else{
        ui->sm_lbl_msg->setText(errorMsg(QString("Unknown issue, failed to add items!")));
    }
}

void StockManagerScreen::on_sm_pb_cancel_clicked()
{
    // close without saving
    this->clearScreen();
    this->close();
}


