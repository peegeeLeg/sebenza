-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2018 at 12:09 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clocking_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `date_and_time` date NOT NULL,
  `activity` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `staff_id`, `date_and_time`, `activity`) VALUES
(1, 1, '2018-08-23', 'yebo'),
(2, 1, '2018-08-24', 'texting');

-- --------------------------------------------------------

--
-- Table structure for table `assigned_patients`
--

CREATE TABLE `assigned_patients` (
  `id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `assigned_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assigned_patients`
--

INSERT INTO `assigned_patients` (`id`, `patient_id`, `staff_id`, `assigned_date`) VALUES
(21, 2, 2, '2018-09-01'),
(28, 1, 2, '2018-09-01'),
(29, 4, 2, '2018-09-01'),
(30, 5, 2, '2018-09-01'),
(31, 6, 2, '2018-09-01'),
(33, 7, 2, '2018-09-01');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time_in` time NOT NULL,
  `time_out` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `staff_id`, `date`, `time_in`, `time_out`) VALUES
(1, 2, '2018-09-13', '11:11:23', '19:22:15'),
(2, 3, '2018-09-28', '12:27:10', '22:13:16'),
(3, 1, '2018-09-06', '19:25:44', '07:15:06');

-- --------------------------------------------------------

--
-- Table structure for table `available_leave_days`
--

CREATE TABLE `available_leave_days` (
  `id` int(11) NOT NULL,
  `available_leave_days` int(11) NOT NULL,
  `allowed_leave_days` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `leave_type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `available_leave_days`
--

INSERT INTO `available_leave_days` (`id`, `available_leave_days`, `allowed_leave_days`, `staff_id`, `leave_type`) VALUES
(4, 22, 22, 1, 'Annual'),
(5, 50, 50, 1, 'Sick'),
(6, 5, 5, 1, 'Family Responsibility'),
(7, 10, 10, 1, 'Study'),
(8, 22, 22, 2, 'Annual'),
(9, 50, 50, 2, 'Sick'),
(10, 5, 5, 2, 'Family Responsibility'),
(11, 10, 10, 2, 'Study'),
(12, 22, 22, 3, 'Annual'),
(13, 50, 50, 3, 'Sick'),
(14, 5, 5, 3, 'Family Responsibility'),
(15, 10, 10, 3, 'Study');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `serial` text NOT NULL,
  `location` text NOT NULL,
  `status` text NOT NULL,
  `action` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `name`, `serial`, `location`, `status`, `action`, `comment`) VALUES
(1, 'Thermometer', '1233', 'Room_1', 'In_use', 'Recycle', 'Worn out'),
(2, 'Thermometer', '1233', 'Room_1', 'In_use', 'Recycle', 'Worn out'),
(3, 'Bed', '6543', 'Room_1', 'Storage', 'Allocate', 'good'),
(4, 'Linen', '7896', 'Room_1', 'Laundry', 'Allocate', 'Dirty'),
(5, 'Linen', '7896', 'Room_1', 'Laundry', 'Allocate', 'Dirty'),
(6, 'Oxy_Meter', '4567', 'Room_1', 'Disposed', 'Dispose', 'Broken'),
(7, 'Oxy_Meter', '4567', 'Room_1', 'Disposed', 'Dispose', 'Broken'),
(8, 'Oxy_Meter', '4567', 'Room_1', 'Disposed', 'Recycle', 'Broken'),
(9, 'Oxy_Meter', '4567', 'Room_4', 'Disposed', 'Recycle', 'Broken');

-- --------------------------------------------------------

--
-- Table structure for table `leave_days`
--

CREATE TABLE `leave_days` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `motivation` text NOT NULL,
  `type_of_leave` varchar(40) NOT NULL,
  `start_date` varchar(10) NOT NULL,
  `end_date` varchar(10) NOT NULL,
  `approval_status` varchar(10) NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_days`
--

INSERT INTO `leave_days` (`id`, `staff_id`, `motivation`, `type_of_leave`, `start_date`, `end_date`, `approval_status`) VALUES
(1, 2, 'asdasd', 'Study', '09/01/2018', '09/09/2018', 'Approved'),
(2, 1, 'qweqweqwe', 'Sick', '09/13/2018', '09/14/2018', 'Pending'),
(3, 1, 'qw3erqweqwe', 'Annual', '09/01/2018', '09/04/2018', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `medication`
--

CREATE TABLE `medication` (
  `id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `medicine` varchar(80) NOT NULL,
  `dosage` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medication`
--

INSERT INTO `medication` (`id`, `patient_id`, `medicine`, `dosage`) VALUES
(1, 2, 'Allergex', '67'),
(2, 2, 'Panado', '76'),
(3, 2, 'Tramal', '89'),
(4, 1, 'Panado', '87'),
(5, 2, 'Panado', '45 ml'),
(6, 2, 'Dispring', '89 l'),
(7, 5, 'Dispring', '90'),
(8, 4, 'Tramal', '754'),
(9, 6, 'Allergex', '67'),
(10, 4, 'Allergex', '345');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `surname` text NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` text,
  `marital_status` text,
  `id_number` text,
  `email` text,
  `contact_number` text,
  `occupation` text,
  `company` text,
  `ethnic_group` text,
  `address` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `name`, `surname`, `date_of_birth`, `gender`, `marital_status`, `id_number`, `email`, `contact_number`, `occupation`, `company`, `ethnic_group`, `address`) VALUES
(1, 'sagdsgh', 'gsavhgdgh', '2013-05-02', 'Male', 'Married', '9999999999999', 'p.legodi@yahoo.com', '3412123', 'fgdgfdgf', 'czdsfd', 'White', 'zSASD'),
(2, 'Sello', 'popo', '1999-09-23', 'Male', 'Divorced', '5434356673677', 'example@yahoo.com', '0987654321', 'yebo', 'reto', 'Coloured', '24353 dede street'),
(4, 'John', 'Doe', '0000-00-00', 'Male', NULL, '', '', '', '', '', '', ''),
(5, 'John', 'Doe', '0000-00-00', 'Male', '', '', '', '', '', '', '', ''),
(6, 'John', 'Doe', '0000-00-00', 'Male', '', '', '', '', '', '', '', ''),
(7, 'Rapu', 'Thapu', '0000-00-00', '<< Select Gender >>', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `surname` text NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` text,
  `id_number` text,
  `contact_number` text,
  `designation` text,
  `ethnic_group` text,
  `address` text,
  `employee_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `user_id`, `name`, `surname`, `date_of_birth`, `gender`, `id_number`, `contact_number`, `designation`, `ethnic_group`, `address`, `employee_number`) VALUES
(1, 1, 'Pee', 'Legodi', '1999-02-23', 'Male', '1234567890987', '1234567890', 'Admin', 'Black', 'srew', '1111'),
(2, 2, 'Betty', 'Mabuza', '1999-02-13', 'Female', '1234567890987', '0987654321', 'Nurse', 'Black', 'admin street', '2222'),
(3, 3, 'Martha', 'Zulu', '1989-09-12', 'Female', '8734682372383', '012 548 485', 'Matron', 'Coloured', 'asdsd', '3333');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `user_type`) VALUES
(1, '1111', 'e00cf25ad42683b3df678c61f42c6bda', 'Admin'),
(2, '2222', 'dc9519338dfac91380ea7897cd19e3b0', 'Nurse'),
(3, '3333', '84a6024592884c26126e54a3201b65cb', 'Matron'),
(7, '4444', '5a105e8b9d40e1329780d62ea2265d8a', 'Nurse');

-- --------------------------------------------------------

--
-- Table structure for table `vital_logs`
--

CREATE TABLE `vital_logs` (
  `id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `vital` varchar(60) NOT NULL,
  `amount` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vital_logs`
--

INSERT INTO `vital_logs` (`id`, `patient_id`, `vital`, `amount`) VALUES
(1, 1, 'Blood Pressure', '78'),
(2, 2, 'Pulse', '87 cc'),
(3, 1, 'Pulse', '45 Pa'),
(4, 1, 'Raspiration', '56 mph'),
(5, 1, 'Pulse', '74'),
(6, 5, 'Saturation', '566');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `assigned_patients`
--
ALTER TABLE `assigned_patients`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `available_leave_days`
--
ALTER TABLE `available_leave_days`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `leave_days`
--
ALTER TABLE `leave_days`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `medication`
--
ALTER TABLE `medication`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `vital_logs`
--
ALTER TABLE `vital_logs`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assigned_patients`
--
ALTER TABLE `assigned_patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `available_leave_days`
--
ALTER TABLE `available_leave_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `leave_days`
--
ALTER TABLE `leave_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `medication`
--
ALTER TABLE `medication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `vital_logs`
--
ALTER TABLE `vital_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
