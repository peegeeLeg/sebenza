#ifndef SYSTEM_USER_H
#define SYSTEM_USER_H

#include <SystemShare.h>
#include <QString>


/*!
 * \brief The User class - Singleton pattern
 */
class SystemUser
{
public:
  /*!
   * \brief Instance
   * \return
   */
  static SystemUser* Instance();

  /*!
   * \brief getName
   * \return name string
   */
  static QString getName(){return user_details_->name;}
  /*!
   * \brief getName
   * \return name string
   */
  static int getId(){return user_details_->id;}

  /*!
   * \brief getSurname
   * \return surname string
   */
  static QString getSurname(){return user_details_->surname;}

  /*!
   * \brief getEmail
   * \return email string
   */
  static QString getEmail(){return user_details_->email_address;}

  /*!
   * \brief getContactNumber
   * \return contact_number string
   */
  static QString getContactNumber(){return user_details_->contact_number;}

  /*!
   * \brief getUserType
   * \return user type
   */
  static userType getUserType(){return user_details_->user_type;}

  /*!
   * \brief getUserType
   * \return true if login was granted
   *         false if login was denied
   */
  static bool getLoginStatus(){return login_status_;}

  /*!
   * \brief setName
   * \param name
   */
  static void setName(QString name){user_details_->name = name;}

  /*!
   * \brief setName
   * \param name
   */
  static void setId(int id){user_details_->id = id;}


  /*!
   * \brief setSurname
   * \param surname
   */
  static void setSurname(QString surname){
    user_details_->surname = surname;
  }

  /*!
   * \brief setEmail
   * \param email
   */
  static void setEmail(QString email){
    user_details_->email_address = email;
  }

  /*!
   * \brief setContactNumber
   * \param contact_number
   */
  static void setContactNumber(QString contact_number){
    user_details_->contact_number = contact_number;
  }

  /*!
   * \brief setUserType
   * \param user_type
   */
  static void setUserType(userType user_type){
    user_details_->user_type = user_type;
  }

  /*!
   * \brief setLoginStatus
   *
   * \param decision
   */
  static void setLoginStatus(bool status){
    login_status_ = status;
  }

  static RecipientsDetails  current_recipient_details;
  static StaffDetails       current_staff_details;
  static SchoolDetails      current_school_search_range;
  static reportType         current_report_selected;

private:
  /*!
   * \brief User default constructor
   */
  SystemUser(){}

  /*!
   * \brief User copy constructor
   */
  SystemUser(SystemUser const&){}

  /*!
   * \brief operator = assignment operator
   * \return
   */
  SystemUser& operator=(SystemUser const&);

  static SystemUser* m_pInstance;

  // User characteristics
  static UserDetails*       user_details_;
  static bool               login_status_;
};

#endif // SYSTEM_USER_H
