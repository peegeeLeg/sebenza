#ifndef REPORT_DATE_PICKER_H
#define REPORT_DATE_PICKER_H

#include <QDialog>
#include <distribution_activity_report.h>


namespace Ui {
class ReportDatePicker;
}

class ReportDatePicker : public QDialog
{
  Q_OBJECT

public:
  explicit ReportDatePicker(QWidget *parent = 0);
  ~ReportDatePicker();

private slots:
  void on_rdp_pb_select_clicked();

  void on_rdp_pb_cancel_clicked();

private:
  Ui::ReportDatePicker *ui;
  // Distribution report screen
  DistributionActivityReport* newDistributionAReport;

  QDate t_from;
  QDate t_to;
};

#endif // REPORT_DATE_PICKER_H
