#include "import_csv.h"
#include "ui_import_csv.h"
#include <fstream>

ImportCSV::ImportCSV(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ImportCSV)
{
  ui->setupUi(this);

  // clear text field
  ui->icsv_tb_text_browser->clear();

  // format expected
  QFont serifFont("Times", 6, QFont::Bold);
//  ui->icsv_lbl_message->setFont(serifFont);
// ui->icsv_lbl_message->setText(" Region Name | District Name	| Education Coordinator |	EC Contact Number |	DSD Coordinator |"
//                                   "	DSD Contact Number |	School Name |	School Address |	School Contact Person Name |"
//                                   "	School Contact Person Surname |	School Contact Number");
  ui->icsv_lbl_message->setText(successMsg("Select excel file to import file"));

}

ImportCSV::~ImportCSV()
{
  delete ui;
}

void ImportCSV::on_icsv_pb_browse_clicked()
{
  fileName = QFileDialog::getOpenFileName(this,
      tr("Open Excel File"), "c:", tr("Excel Files (*.csv)"));

  ui->icsv_tb_text_browser->setText(fileName);
  qDebug() << fileName;
}

void ImportCSV::on_icsv_pb_cancel_clicked()
{
  this->close();
}

void ImportCSV::on_icsv_pb_import_clicked()
{
  std::ifstream file(fileName.toStdString().c_str() );

  int                 temp_id;
  QString             temp_string;
  std::string         line;
  QList<QStringList>  valuesList;
  QStringList         valueList;

  SchoolDetails*       temp_school_details = new SchoolDetails;
  RegionDetails*       temp_region_details = new RegionDetails;

  int row = 0;
  bool done = false;
  qDebug()<<" ImportCSV::on_icsv_pb_import_clicked() - Got the file";
  while(std::getline(file, line))
  {
      qDebug()<<" ImportCSV::on_icsv_pb_import_clicked() - IN the file";

    temp_string = QString::fromStdString(line);

    // - check if number of cols is valid
    if(temp_string.split(",").count() != 11 && row == 0)
    {
      // -- break with error
      ui->icsv_lbl_message->setText(errorMsg("Invalid File Format - contact Administrator!"));
      qDebug()<<" ImportCSV::on_icsv_pb_import_clicked() - Invalid File Format - contact Administrator!";

      break;
    }

    // "0Region Name | 1District Name	| 2Education Coordinator |	3EC Contact Number |	4DSD Coordinator |"
    // "	5DSD Contact Number |	6School Name |	7School Address |	8School Contact Person Name |"
    // "	9School Contact Person Surname |	10School Contact Number");

    if(row > 0)
    {
      // -- grab data
      valueList = temp_string.split(",");
      valuesList.append(temp_string.split(","));

      // -- create region details
      temp_region_details->region              = valueList.at(0);
      temp_region_details->district            = valueList.at(1);
      temp_region_details->education_coord     = valueList.at(2);
      temp_region_details->education_coord_tel = valueList.at(3);
      temp_region_details->dsd_coord           = valueList.at(4);
      temp_region_details->dsd_coord_tel       = valueList.at(5);

      // -- store region database
      DatabaseController::active_db_handle_->addRegion(temp_region_details);


      // -- now get the id of the just stored region
      temp_id = 0;
      DatabaseController::active_db_handle_->getIdByDistricName(temp_region_details->district, temp_id);

      // -- create school details
      temp_school_details->region_id                 = temp_id;
      temp_school_details->name                      = valueList.at(6);
      temp_school_details->address                   = valueList.at(7);
      temp_school_details->contact_person_name       = valueList.at(8);
      temp_school_details->contact_person_surname    = valueList.at(9);
      temp_school_details->contact_person_tel        = valueList.at(10);
      temp_school_details->num_of_learners           = 0;
      temp_school_details->num_of_learners_enrolled  = 0;

      // -- store school details
      DatabaseController::active_db_handle_->addSchool(temp_school_details);
    }
    row++;

    qDebug()<<" ImportCSV::on_icsv_pb_import_clicked() - row count:"<<row;

    done = true;

  }
  if(done)
  ui->icsv_lbl_message->setText(successMsg("Import Successful!"));
  else
      ui->icsv_lbl_message->setText(errorMsg("Import Faliled!"));

}
