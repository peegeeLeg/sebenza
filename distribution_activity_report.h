#ifndef DISTRIBUTION_ACTIVITY_REPORT_H
#define DISTRIBUTION_ACTIVITY_REPORT_H

#include <QDialog>
#include <QDateTime>
#include <SystemShare.h>
#include <database_controller.h>
#include <excel_export_helper.h>


namespace Ui {
class DistributionActivityReport;
}

class DistributionActivityReport : public QDialog
{
  Q_OBJECT

public:
  explicit DistributionActivityReport(QDate t_from,
                                      QDate t_to,
                                      QWidget *parent = 0);
  ~DistributionActivityReport();

  void generateDistributionDetailsReport();
  void generateDistributionSummaryReport();
  void generateSystemUsersListReport();
  void generateRecipientsListReport();
  void generateRegionDeliveriesReport();
  void generateSchoolDeliveriesReport();

private slots:
  void on_commandLinkButton_clicked();

private:
  Ui::DistributionActivityReport *ui;
  ExcelExportHelper helper;
  QString current_html_string;
  QString current_report_name;
  QDate from_date;
  QDate to_date;
};

#endif // DISTRIBUTION_ACTIVITY_REPORT_H
