#ifndef IMPORT_CSV_H
#define IMPORT_CSV_H

#include <QDialog>
#include <QFileDialog>
#include <SystemShare.h>
#include <database_controller.h>

namespace Ui {
class ImportCSV;
}

class ImportCSV : public QDialog
{
  Q_OBJECT

public:
  explicit ImportCSV(QWidget *parent = 0);
  ~ImportCSV();

private slots:
  void on_icsv_pb_browse_clicked();

  void on_icsv_pb_cancel_clicked();

  void on_icsv_pb_import_clicked();

private:
  Ui::ImportCSV *ui;
  QString fileName;
};

#endif // IMPORT_CSV_H
