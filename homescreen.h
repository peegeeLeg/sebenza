#ifndef HOMESCREEN_H
#define HOMESCREEN_H

#include <QMainWindow>
#include <recipients.h>
#include <SystemShare.h>
#include <fingerprintscanner.h>
#include <UserAdministration.h>
#include <database_controller.h>
#include <distribution_activity_report.h>
#include <import_csv.h>

// Screens
#include <login_screen.h>
#include <identification_screen.h>
#include <report_date_picker.h>
#include <stock_manager_screen.h>


/*!
 *
 */
namespace Ui {
class HomeScreen;
}

/*!
 * \brief The HomeScreen class
 */
class HomeScreen : public QMainWindow
{
  Q_OBJECT

public:
  explicit HomeScreen(QWidget *parent = 0);
  ~HomeScreen();

  /*!
   * \brief ApplicationLogin - Main Login Screen setup and verify
   * \return
   */
  void ApplicationLogin();

public slots:

  /*!
   * \brief on_login
   * \param loginState
   * \param typeUser
   */
  void on_login(bool loginState, userType typeUser);

private slots:
  void on_actionConfiguration_triggered();

  void on_actionLogout_triggered();

  void on_actionAdd_Recipient_triggered();

  void on_actionRemove_Recipient_triggered();

  void on_actionEdit_Recipient_triggered();

  void on_actionAuthenticate_Recipient_triggered();

  void on_actionNew_triggered();

  void on_actionEdit_triggered();

  void on_hs_tb_parent_sync_clicked();

  void on_hs_tb_authenticate_clicked();

  void on_actionRemove_User_triggered();

  void on_action_Add_New_Group_triggered();

  void on_action_Edit_Existing_Group_triggered();

  void on_actionRemove_Existing_Group_triggered();

  void on_hs_report_distribution_activity_details_menu_item_triggered();

  void on_hs_report_distribution_activity_summary_menu_item_triggered();

  void on_action_Import_CSV_triggered();

  void on_actionSystem_USer_Listing_triggered();

  void on_actionStock_Management_triggered();

  void on_actionRegion_Deliveries_triggered();

  void on_actionSchool_Deliveries_triggered();

private:
  Ui::HomeScreen *ui;

  // login Screen
  Login_Screen*               newLoginScreen;
  // Identify  Recipient
  IdentificationScreen*       newIdentificationScreen;
  // input form
  UserAdministration*         newUserAdmin;
  // recipients form
  Recipients*                 newRecipientsScreen;
  // fingerprint scanner window
  FingerprintScanner*         newFingerprintScannerWindow;
  // Distribution report date picker screen
  ReportDatePicker*           newReportDatePicker;
  // General Report screen
  DistributionActivityReport* newReportScreen;
  // Stock Manager screen
  StockManagerScreen*         newStockManagerScreen;
  // Excel CSV import screen
  ImportCSV*                  newImportScreen;

  // bool states
  bool                        login_state;
  bool                        db_conn_state_;
  // enum
  userType                    user_type;
  // db controller
  DatabaseController*         db_controller_;


};

#endif // HOMESCREEN_H
